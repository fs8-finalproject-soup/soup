import React from 'react'
import succesTransaction from '../../assets/EmailConfirm.png'
import { Typography, Button } from '@mui/material'
import HomeIcon from '@mui/icons-material/Home';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import { useHistory } from 'react-router-dom';

const SuccessPurcase = () => {

  const history = useHistory();
  
  const handleOpenInvoice = () => {
    history.push("/invoice");
  };

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
        <img src={succesTransaction} alt="." style={{marginTop:'54px'}} />
        <Typography style={{marginTop:'44px', color:'#226957', fontSize:'24px'}}>Purchase Successfully</Typography>
        <Typography style={{marginTop:'8px', fontSize:'16px', color:'#4F4F4F'}}>Horray!! Let's cook and become a professional cheff</Typography>
        <div>
          <Button startIcon={<HomeIcon/>} style={{marginTop:'40px' ,background:'#FFFFFF', width:'200px', height:'50px', padding:'16px 24px', borderRadius:'6px', color:'#5B4947'}}><Typography style={{fontSize:'15px'}}>Back to Home</Typography></Button>
          <Button startIcon={<ArrowRightAltIcon/>} style={{marginTop:'40px', marginLeft:'24px' ,background:'#FABC1D', width:'200px', height:'50px', padding:'16px 24px', borderRadius:'6px', color:'#ffffff'}}><Typography style={{fontSize:'15px'}} onClick={handleOpenInvoice}>Open Invoice</Typography></Button>
        </div>
      </div>
    </div>
  )
}

export default SuccessPurcase
