import React from 'react';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import InputTextMessage from '../../component/inputTextMessage';
import { Box, Grid } from '@mui/material';
import { useHistory } from 'react-router-dom';

const CreatePass = () => {

  const history = useHistory();

  const handleLogin = () => {
    history.push('/login');
  };

  return (
    <div>
      <Container maxWidth="sm">
        <Box sx={{
          marginTop: 20,
          display: "flex",
          alignItems: "center",
          flexDirection: "column"
        }}>
          <Grid container justifyContent="flex-start" alignItems="center" sx={{ mb: 2 }}>
            <Grid item xs={12}>
              <Typography style={{
                fontFamily: 'Montserrat', fontSize: '24px',
                fontWeight: 400,
                lineHeight: '29px',
                letterSpacing: '0em',
                textAlign: 'left',
              }}>
                Create Password
              </Typography>
            </Grid>
          </Grid>
          <InputTextMessage
            id="newpassword"
            label="New Password"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            autoFocus
            name="password"
            type="password"
          />
          <InputTextMessage
            id="Confirmnewpassword"
            label="Confirm New Password"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            autoFocus
            name="password"
            type="password"
          />

          <div style={{ display: 'flex', alignSelf: 'flex-end', marginTop: '10px' }}>
            <Button
              type="submit"
              variant="contained"
              size="medium"
              sx={{
                mt: 3, mb: 1, marginRight: '10px', backgroundColor: '#FFFFFF',
                color: 'black', textTransform: 'none', borderRadius: '5px', width: "140px", height: "38px"
              }}
            >
              Cancel
            </Button>
            <Box sx={{ width: '24px' }} />
            <Button
              type="submit"
              variant="contained"
              size="medium"
              onClick={handleLogin}
              sx={{
                mt: 3, mb: 1, 
                backgroundColor: '#EA9E1F',
                color: 'black', 
                textTransform: 'none', 
                borderRadius: '5px', 
                width: "140px", 
                height: "38px",
                marginBottom: '175px'
              }}
            >
              Submit
            </Button>
          </div>
        </Box>
      </Container>
    </div>
  )
}

export default CreatePass;
