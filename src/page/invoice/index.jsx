import { Button, Link, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, styled, tableCellClasses } from '@mui/material'
import React from 'react'
import { useHistory } from 'react-router-dom';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#5B4947',
        color: theme.palette.common.white
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14
    }
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}))

function createList(invoice, date, tcourse, tprice) {
    return { invoice, date, tcourse, tprice };
}

const rows = [
    createList('123456789', '12 July 2022', '1', '450000'),
    createList('987654321', '23 October 2022', '3', '1800000'),
    createList('346964384', '22 March 2022', '1', '500000'),
    createList('492375535', '19 December 2022', '1', '550000'),
    createList('876358732', '8 February 2022', '2', '1000000')
]

const Invoice = () => {
    const history = useHistory()

    const handleDetailInvoice = ()=>{
        history.push('/detailinvoice')
    }

    return (
        <Stack
            direction="column"
            alignItems="flex-start"
            spacing={2}
            width="90%"
            mx="auto"
            marginTop="30px"
        >
            <Typography sx={{ fontWeight: '600', fontSize: '16px' }}>
                <Link href='/' style={{ textDecoration: 'none' }}>Home </Link>
                {">"} Invoice
            </Typography>
            <Typography sx={{ fontWeight: '600', fontSize: '20px' }}>
                Menu Invoice
            </Typography>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align='center'>No</StyledTableCell>
                            <StyledTableCell align='center'>No. Invoice</StyledTableCell>
                            <StyledTableCell align='center'>Date</StyledTableCell>
                            <StyledTableCell align='center'>Total Course</StyledTableCell>
                            <StyledTableCell align='center'>Total Price</StyledTableCell>
                            <StyledTableCell align='center'>Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((item, index) => (
                            <StyledTableRow key={index}>
                                <StyledTableCell align='center'>{parseInt(index) + 1}</StyledTableCell>
                                <StyledTableCell align='center'>{item.invoice}</StyledTableCell>
                                <StyledTableCell align='center'>{item.date}</StyledTableCell>
                                <StyledTableCell align='center'>{item.tcourse}</StyledTableCell>
                                <StyledTableCell align='center'>{`IDR ${parseInt(item.tprice).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}`}</StyledTableCell>
                                <StyledTableCell align='center'>
                                    <Button onClick={handleDetailInvoice} style={{
                                        borderRadius: '8px',
                                        backgroundColor: '#FABC1D',
                                        borderColor: '#FABC1D',
                                        color: '#5B4947',
                                        width: '175px',
                                        height: '40px'
                                    }}>Details</Button>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Stack>
    )
}

export default Invoice
