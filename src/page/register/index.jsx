import React from 'react'
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import InputTextMessage from '../../component/inputTextMessage'
import { Box, Link, Grid } from '@mui/material';
import { useHistory } from 'react-router-dom';
import axios from 'axios';


const Register = () => {

    const history = useHistory();
  
    const handleSignUp = () => {
      axios.post("http://localhost:58682/api/user/register", {
        name: "osas",
        email: "osas123@gmail.com",
        password: "osas",
        role: "user"
      }).then(function (response) {
        if (response.data === "Register Successfully") {
          history.push('/emailconfirm');
        }
        console.log(response.data)
      }).catch(function (error) {
        console.log(error)
      })
    };

  return (
    <div>
      
      <Container maxWidth="sm">
        <Box sx={{
          marginTop: 10,
          display: "flex",
          alignItems: "center",
          flexDirection: "column"
        }}>
          <Grid container justifyContent="flex-start" alignItems="center" sx={{ mb: 2 }}>
            <Grid item xs={12}>
              <Typography sx={{
                fontFamily: 'Montserrat',
                fontSize: '24px',
                fontWeight: '500',
                lineHeight: '29.26px',
                letterSpacing: '0em',
                textAlign: 'left',
                width: '531px',
                height:'29px',
                color: '#5B4947'
              }}>
                 Are you ready a become a professional cheff?
              </Typography>
              <Typography sx={{
                fontFamily: 'Montserrat',
                fontSize: '16px',
                fontWeight: '400',
                lineHeight: '19.5px',
                letterSpacing: '0em',
                textAlign: 'left',
                width: '153px',
                height:'20px',
                color: '#4F4F4F'
              }}>
                Please register first
              </Typography>
            </Grid>
          </Grid>
          <InputTextMessage
            id="nama"
            label="Nama"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            autoFocus
            name="nama"
            type="text"
          />
          <InputTextMessage
            id="email"
            label="Email"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            autoFocus
            name="email"
            type="text"
          />
          <InputTextMessage
            id="password"
            label="Password"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            autoFocus
            name="password"
            type="password"
          />
          <InputTextMessage
            id="confirmpassword"
            label="Confirm Password"
            variant="outlined"
            fullWidth
            required
            margin="normal"
            autoFocus
            name="password"
            type="password"
          />
          <Grid container justifyContent="space-between" alignItems="center">
          </Grid>
          <Button
            type="submit"
            variant="contained"
            size="medium"
            sx={{ mt: 3, mb: 1, alignSelf: 'flex-end', backgroundColor: '#EA9E1F', 
            color: 'black',textTransform: 'none', borderRadius: '5px', width:"140px" }}
            onClick={handleSignUp}>
            Sign Up
          </Button>
          <Grid container justifyContent="center" alignItems="center" sx={{ mt: 7, mb: 1, alignSelf: 'flex-end' }}>
            <Grid item>
              <Typography sx={{
                fontFamily: 'Montserrat',
                fontSize: '16px',
                fontWeight: '400',
                lineHeight: '29.26px',
                letterSpacing: '0em',
                textAlign: 'left',
                width: '209px',
                height:'20px',
                color: 'black'
              }}>
                Have account?
                <Link href="/login" color="primary" style={{ marginLeft: '2px'}}>
                  Login here
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </div>
  )
}

export default Register;
