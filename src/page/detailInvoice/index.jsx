import { Box, Link, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, styled, tableCellClasses } from '@mui/material'
import React from 'react'

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#5B4947',
        color: theme.palette.common.white
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14
    }
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}))

function createList(ncourse, type, schedule, price) {
    return { ncourse, type, schedule, price };
}

const rows = [
    createList('Tom Yum Thailand', 'Asian', '12 July 2022', '250000'),
    createList('Strawberry Float', 'Dessert', '23 October 2022', '200000'),
]

const detailInvoice = {
    noInvoice: '123456789',
    date: '12 July 2022'
}

const index = () => {
    return (
        <Stack
            direction="column"
            alignItems="flex-start"
            spacing={2}
            width="90%"
            mx="auto"
            marginTop="30px"
        >
            <Typography sx={{ fontWeight: '600', fontSize: '16px' }}>
                <Link href='/' style={{ textDecoration: 'none' }}>Home</Link>
                {" > "}
                <Link href='/invoice' style={{ textDecoration: 'none' }}>Invoice</Link>
                {" > "}
                Detail Invoice
            </Typography>
            <Typography sx={{ fontWeight: '600', fontSize: '20px' }}>
                Detail Invoice
            </Typography>
            <Typography sx={{ fontWeight: '500', fontSize: '18px' }}>
                No. Invoice : {detailInvoice.noInvoice}
            </Typography>
            <Box sx={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <Typography sx={{ fontWeight: '500', fontSize: '18px' }}>
                    Date : {detailInvoice.date}
                </Typography>
                <Typography sx={{ fontWeight: '700', fontSize: '18px' }}>
                    Total Price IDR {rows.reduce((a,v)=>a+parseInt(v.price),0).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}
                </Typography>
            </Box>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align='center'>No</StyledTableCell>
                            <StyledTableCell align='center'>Course Name</StyledTableCell>
                            <StyledTableCell align='center'>Type</StyledTableCell>
                            <StyledTableCell align='center'>Schedule</StyledTableCell>
                            <StyledTableCell align='center'>Price</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((item, index) => (
                            <StyledTableRow key={index}>
                                <StyledTableCell align='center'>{parseInt(index) + 1}</StyledTableCell>
                                <StyledTableCell align='center'>{item.ncourse}</StyledTableCell>
                                <StyledTableCell align='center'>{item.type}</StyledTableCell>
                                <StyledTableCell align='center'>{item.schedule}</StyledTableCell>
                                <StyledTableCell align='center'>{`IDR ${parseInt(item.price).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}`}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Stack>
    )
}

export default index
