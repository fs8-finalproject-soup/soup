import React from 'react'
import Content from '../../component/content'
import { Box, Stack, Typography } from '@mui/material'
import Banner3 from '../../component/benner/Banner3'
import { useParams} from 'react-router-dom';
// import axios from "axios"
import { useEffect, useState } from 'react';

const Menu = () => {

const {category_name} = useParams();
const [data, setData] = useState([null]);


const apiUrl = process.env.REACT_APP_GET_CATEGORY_NAME;

const [error, setError] = useState(null);

useEffect(() => {
  const fetchData = async () => {
    try {
      const response = await fetch(apiUrl + `?category_name=${category_name}`); // Replace with your API URL
      const responseData = await response.json();
      setData(responseData);
    } catch (error) {
      console.error("Error fetching data", error);
    }
  };

  fetchData();
}, [apiUrl, category_name]);


  return (
    <Stack spacing={7} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Banner3 />
      <Box sx={{ width: '90%' }}>
        <Typography sx={{ fontWeight: '600', fontSize: '24px' }}>
         {data.category_name}
        </Typography>
        <Typography sx={{ fontWeight: '400', fontSize: '16px' }}>
          {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. */}

          {data.description}
        </Typography>
      </Box>
      <Typography
                sx={{
                    fontSize: '32px',
                    fontWeight: '600',
                }}>
                Another menu in this class
            </Typography>
      <Content />
    </Stack>
  )
}

export default Menu
