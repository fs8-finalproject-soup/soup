import React from 'react';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import InputTextMessage from '../../component/inputTextMessage';
import { Box, Grid } from '@mui/material';
import { useHistory } from 'react-router-dom';

const ResetPass = () => {

    const history = useHistory();

    const handleLogin = () => {
        history.push('/login');
    };

    const handleConfirm = () => {
        history.push('/createpass');
    };

    return (
        <div>

            <Container maxWidth="sm">
                <Box sx={{
                    marginTop: 20,
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column"
                }}>
                    <Grid container justifyContent="flex-start" alignItems="center" sx={{ mb: 5 }}>
                        <Grid item xs={12}>
                            <Typography sx={{
                                fontFamily: 'Montserrat',
                                fontSize: '24px',
                                fontWeight: '400',
                                lineHeight: '29.26px',
                                letterSpacing: '0em',
                                textAlign: 'left',
                                color: '#333333',
                                width: '190px',
                                height: '29px'
                            }}>
                                Reset Password
                            </Typography>
                            <Typography sx={{
                                fontFamily: 'Montserrat',
                                fontSize: '16px',
                                fontWeight: '400',
                                lineHeight: '19.5px',
                                letterSpacing: '0em',
                                textAlign: 'left',
                                color: '##4F4F4F',
                                width: '299px',
                                height: '20px'
                            }}>
                                Send OTP code to your email address
                            </Typography>
                        </Grid>
                    </Grid>
                    <InputTextMessage
                        id="email"
                        label="Email"
                        variant="outlined"
                        fullWidth
                        required
                        margin="normal"
                        autoFocus
                        name="email"
                        type="text"

                    />
                    <div style={{ display: 'flex', alignSelf: 'flex-end', marginTop: '10px' }}>
                        <Button
                            type="submit"
                            variant="contained"
                            size="medium"
                            onClick={handleLogin}
                            sx={{
                                mt: 3, mb: 1, marginRight: '10px', backgroundColor: '#FFFFFF',
                                color: 'black', textTransform: 'none', borderRadius: '5px', width: "140px", height: "38px"
                            }}
                        >
                            Cancel
                        </Button>
                        <Button
                            type="submit"
                            variant="contained"
                            size="medium"
                            onClick={handleConfirm}
                            sx={{
                                mt: 3, mb: 1, backgroundColor: '#EA9E1F',
                                color: 'black', textTransform: 'none', borderRadius: '5px', width: "140px", height: "38px"
                            }}
                        >
                            Confirm
                        </Button>
                    </div>
                </Box>
            </Container>
        </div>
    )
}

export default ResetPass;
