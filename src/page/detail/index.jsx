import { Box, Button, Typography, Select, InputLabel, MenuItem, FormControl, useMediaQuery } from '@mui/material';
import { useEffect, useState } from 'react';
import Content from '../../component/content';
import { useHistory, useParams} from 'react-router-dom';
import axios from "axios"

// ... (previous imports and components)

const Detail = () => {
  const isMobile = useMediaQuery('(max-width: 768px)');
  const history = useHistory();
  const {id} = useParams();
  const [data, setData] = useState([null]);


  const apiUrl = process.env.REACT_APP_GET_DETAIL_PRODUCT;

  
const [error, setError] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get(apiUrl, {
          params: {
            id: id,
          },
        });
        setData(data);
      } catch (error) {
        console.error("Error fetching data", error);
        setError("Error fetching data. Please try again later.");
      }
    };
  
    fetchData();
  }, [apiUrl, id]);


// Render error message if there's an error
if (error) {
  return <div>{error}</div>;
}

  const handleCheckout = () => {
    history.push('/checkout');
  }

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
       <Box sx={{ display: 'flex', flexDirection: isMobile ? 'column' : 'row', alignItems: 'flex-start', my: '50px', width: '90%' }}>
        <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: isMobile ? 'center' : 'flex-start', mb: isMobile ? '30px' : '0' }}>
          <Box>
            <img src={'/' + data.picture } alt='soup' style={{ width: '100%', maxWidth: '400px', marginRight: isMobile?  '' : '30px' }} />
          </Box>
          <Box sx={{ display: 'flex', flexDirection: 'column', ml: '10px' }}>
            <Typography sx={{ fontWeight: '400', fontSize: isMobile ? '14px' : '16px', color: '#828282', textAlign: isMobile ? 'center' : 'left' }}>
            {data.category}
            </Typography>
            <Typography sx={{ fontWeight: '600', fontSize: isMobile ? '20px' : '24px', color: '#333333', mb: '8px', textAlign: isMobile ? 'center' : 'left' }}>
              {data.name}
            </Typography>
            <Typography sx={{ fontWeight: '600', fontSize: isMobile ? '20px' : '24px', color: '#5B4947', mb: '10px', textAlign: isMobile ? 'center' : 'left' }}>
              IDR {data.price}
            </Typography>
            <FormControl sx={{ minWidth: 80, mb: '10px' }}>
              <InputLabel id="demo-simple-select-autowidth-label">Select</InputLabel>
              <Select
                labelId="demo-simple-select-autowidth-label"
                id="demo-simple-select-autowidth"
                autoWidth
                label="Select"
              >
                <MenuItem value={1}>Tuesday</MenuItem>
                <MenuItem value={2}>Wednesday</MenuItem>
                <MenuItem value={3}>Thursday</MenuItem>
              </Select>
            </FormControl>
            <Box sx={{ display: 'flex', flexDirection: isMobile ? 'column' : 'row', alignItems: isMobile ? 'center' : 'flex-start' }}>
              <Button variant="outlined" component="div"
                style={{
                  borderRadius: '8px',
                  backgroundColor: '#FFFFFF',
                  borderColor: '#5B4947',
                  color: '#5B4947',
                  width: isMobile ? '100%' : '175px',
                  height: '40px',
                  marginRight: isMobile ? '0' : '10px',
                  mb: isMobile ? '10px' : '0',
                  marginBottom: '10px'
                }}
              >
                Add to Cart
              </Button>
              <Button variant="outlined" component="div"
                style={{
                  borderRadius: '8px',
                  backgroundColor: '#FABC1D',
                  borderColor: '#FABC1D',
                  color: '#5B4947',
                  width: isMobile ? '100%' : '175px',
                  height: '40px',
                }}
                onClick={handleCheckout}
              >
                Buy Now
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
      <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'left', paddingX: '60px' }}>
      <Typography sx={{ fontWeight: '600', fontSize: isMobile ? '20px' : '24px', color: '#333333', my: isMobile ? '30px' : '0', textAlign: isMobile ? 'center' : 'left' }}>
        Description
      </Typography>
      <Typography sx={{ fontWeight: '400', fontSize: isMobile ? '14px' : '16px', color: '#333333', my: '15px', textAlign: isMobile ? 'center' : 'left' }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </Typography>
      <Typography sx={{ fontWeight: '400', fontSize: isMobile ? '14px' : '16px', color: '#333333', textAlign: isMobile ? 'center' : 'left' }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </Typography>
    </Box>
      <Typography
        sx={{
          fontSize: '32px',
          fontWeight: '600',
          mt: '30px',
          paddingX: '30px'
        }}>
        Another menu in this class
      </Typography>
      <Content />
    </Box>
  );
};

export default Detail;

