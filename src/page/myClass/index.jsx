import { Box, Grid, Typography } from "@mui/material";
import React from "react";
import { item1, item2, item3, item4, item5 } from '../../assets'

const data = [
    {
        title: 'Rendang',
        category: 'a',
        price: '123000',
        image: [item1]
    },
    {
        title: 'Strawberry Float',
        category: 'b',
        price: '234000',
        image: [item2]
    },
    {
        title: 'Choco Chip',
        category: 'c',
        price: '456000',
        image: [item3]
    },
    {
        title: 'Bolu Cake',
        category: 'd',
        price: '200000',
        image: [item4]
    },
    {
        title: 'Bubur Ayam',
        category: 'e',
        price: '150000',
        image: [item5]
    }

]

export default function MyClass() {
    // const course = useContext(itemContext)
    // const num = 4
    // const filt = course.filter(mycourse => mycourse.id === parseInt(num))

    return (
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            sx={{
                width: "100%",
                minHeight: "800px",
            }}
        >
            {data.map(my => {
                return (
                    <Box
                        sx={{
                            borderBottom: 1,
                            borderColor: 'grey.500',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            width: "70%",
                            minHeight: "20px",
                            marginTop: "20px",
                            paddingBottom: "20px"
                        }}
                    >
                        <img
                            src={my.image}
                            alt={my.title}
                            width="200px"
                        />
                        <Box sx={{ mx: '20px' }}>
                            <Typography
                                gutterBottom
                                variant="span"
                                color="text.secondary"
                                component="div"
                                marginBottom="10px"
                            >
                                {my.category}
                            </Typography>
                            <Typography
                                variant="h6"
                                color="text.primary"
                                marginBottom="10px"
                                sx={{ fontWeight: 600 }}
                            >
                                {my.title}
                            </Typography>
                            <Typography
                                variant="h6"
                                sx={{ fontWeight: 500, fontSize: '20px', color: '#FABC1D' }}
                            >
                                Schedule: ???
                            </Typography>
                        </Box>
                    </Box>
                )
            })}
        </Grid>
    )
}
