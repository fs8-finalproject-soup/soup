import React, { useState } from 'react';
import { Box, Grid, Checkbox, Typography, IconButton, Button,useMediaQuery } from '@mui/material';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
// import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TomYunThailand from '../../assets/TomYunThailand.png';
import Product2Image from '../../assets/Rectangle 12-11.png';
import Product3Image from '../../assets/Rectangle 12-11.png';
import Product4Image from '../../assets/Rectangle 12-12.png';
import Product5Image from '../../assets/Rectangle 12-2.png';
import PaymentModal from '../../component/paymentMethod';

const products = [
  {
    image: TomYunThailand,
    category: 'Asian',
    name: 'Tom Yum Thailand',
    price: 'IDR 450.000',
    deskripsi: 'Schedule: Senin 22-12-2023',
  },
  {
    image: Product2Image,
    category: 'Category 2',
    name: 'Product 2',
    price: 'IDR 250.000',
    deskripsi: 'Description for Product 2',
  },
  {
    image: Product3Image,
    category: 'Category 3',
    name: 'Product 3',
    price: 'IDR 350.000',
    deskripsi: 'Description for Product 3',
  },
  {
    image: Product4Image,
    category: 'Category 4',
    name: 'Product 4',
    price: 'IDR 150.000',
    deskripsi: 'Description for Product 4',
  },
  {
    image: Product5Image,
    category: 'Category 5',
    name: 'Product 5',
    price: 'IDR 550.000',
    deskripsi: 'Description for Product 5',
  },
];



const Checkout = () => {
  const [selectAll, setSelectAll] = useState(false);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const isMobile = useMediaQuery('(max-width: 768px)');

  const handleSelectAllChange = (event) => {
    const checked = event.target.checked;
    setSelectAll(checked);

    // Add your logic for handling the select all functionality here
    const updatedSelectedProducts = checked ? Array.from(Array(products.length).keys()) : [];
    setSelectedProducts(updatedSelectedProducts);
  };

  const handleProductSelect = (event, index) => {
    const checked = event.target.checked;
    const updatedSelectedProducts = checked
      ? [...selectedProducts, index]
      : selectedProducts.filter((selectedIndex) => selectedIndex !== index);

    setSelectedProducts(updatedSelectedProducts);
  };

  const handleDelete = () => {
    // Remove the selected products from the 'products' array
    // const updatedProducts = products.filter((product, index) => !selectedProducts.includes(index));
    // Clear the selected products array
    setSelectedProducts([]);
    // Update the 'products' array after removing the selected products
    // You can handle deletion logic here, e.g., send a request to a server to delete the products.
  };

  const handleDeleteAll = () => {
    // Clear the selected products array
    setSelectedProducts([]);
    // Update the 'products' array to remove all products
    // You can handle deletion logic here, e.g., send a request to a server to delete all products.
  };

  // Calculate the total price based on selected products
  const getTotalPrice = () => {
    const totalPrice = selectedProducts.reduce(
      (acc, currentIndex) => acc + parseFloat(products[currentIndex].price.replace('IDR ', '').replace(',', '.')),
      0
    );
    return totalPrice.toFixed(2);
  };

  const handleOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  return (
    <Box sx={{ marginTop: "100px", marginLeft: '72px', marginRight: '72px', display: 'flex', flexDirection: 'column' }}>
      <Grid container alignItems="center">
        <Grid item>
          <Checkbox checked={selectAll} onChange={handleSelectAllChange} />
        </Grid>
        <Grid item>
          <Typography sx={{ fontFamily: 'Poppins', fontSize: '20px', fontWeight: 400, marginLeft: '12px' }}>
            Pilih Semua
          </Typography>
        </Grid>
        <Grid item xs={14} sm={8} md={10} />
        <Grid item>
          {selectedProducts.length > 0 && (
            <IconButton color="error" aria-label="delete-all" onClick={handleDeleteAll}>
              <DeleteForeverIcon />
            </IconButton>
          )}
        </Grid>
      </Grid>
      <Box>
        {/* Map through products */}
        {products.map((product, index) => (
          <Box key={index} sx={{ display: 'flex', alignItems: 'center', padding: '16px',borderBottom: '1px solid #BDBDBD', borderRadius: '16px', marginTop: '24px', marginLeft: '16px', flex: 1 }}>
            <Checkbox checked={selectedProducts.includes(index)} onChange={(event) => handleProductSelect(event, index)} />
            <img src={product.image} alt="Product Image" style={{ width: '100px', height: '100px', borderRadius: '8px', marginLeft: '16px' }} />
            <Box sx={{ marginLeft: '25px', flex: 1 }}>
              <Typography sx={{ fontFamily: 'Montserrat', fontSize: '16px', fontWeight: 400, color: 'Gray 3' }}>
                {product.category}
              </Typography>
              <Typography sx={{ fontFamily: 'Montserrat', fontSize: '24px', fontWeight: 600 }}>
                {product.name}
              </Typography>
              <Typography sx={{ fontFamily: 'Montserrat', fontSize: '16px', fontWeight: 400, color: 'Gray 2' }}>
                {product.deskripsi}
              </Typography>
              <Typography sx={{ fontFamily: 'Montserrat', fontSize: '20px', fontWeight: 600, color: '#FABC1D' }}>
                {product.price}
              </Typography>
            </Box>
            <IconButton color="error" aria-label="delete" onClick={handleDelete}>
              <DeleteForeverIcon />
            </IconButton>
          </Box>
        ))}

        {/* Display total price and Pay Now Button */}
        <Box sx={{ marginTop: '126px', marginLeft: '72px', marginRight: '72px', display: 'flex', flexDirection: isMobile ? 'column' : 'row' }}>
      <Grid container alignItems="center">
        <Grid item xs={isMobile ? 12 : 10}>
          <Typography sx={{ fontFamily: 'Poppins', fontSize: isMobile ? '16px' : '20px', fontWeight: 600, textAlign: isMobile ? 'center' : 'left', marginBottom: isMobile ? '10px' : '0' }}>
            Total Price: IDR {getTotalPrice()}
          </Typography>
        </Grid>
        <Grid item xs={isMobile ? 12 : 2}>
          <Button variant="contained" color="primary"
            sx={{
              backgroundColor: "#FABC1D",
              fontFamily: "Montserrat",
              minWidth: isMobile ? "100%" : "130px",
            }} onClick={handleOpenDialog}>
            Pay Now
          </Button>
        </Grid>
      </Grid>
    </Box>
      </Box>

      {/* Payment Modal */}
      <PaymentModal openDialog={openDialog} handleCloseDialog={handleCloseDialog} />
    </Box>
  );
};

export default Checkout;
