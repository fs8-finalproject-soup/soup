import React from 'react';
import { Stack, Typography, useMediaQuery } from '@mui/material';
import Banner1 from '../../component/benner/Banner1';
import Banner2 from '../../component/benner/Banner2';
import Trend from '../../component/trend';
import Content from '../../component/content';
import Category from '../../component/category';

const Home = () => {
  const isMobile = useMediaQuery('(max-width: 600px)'); // Gunakan lebar layar 600px sebagai breakpoint mobile

  return (
    <Stack
      spacing={isMobile ? 3 : 7}
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center', // Center the content horizontally
      }}
    >
      <Banner1 />
      <Trend />

      <Typography variant="h2" sx={{ fontSize: isMobile ? '24px' : '32px', fontWeight: '600' }}>
        More professional class
      </Typography>
      <Content />

      <Banner2 />

      <Typography variant="h2" sx={{ fontSize: isMobile ? '24px' : '32px', fontWeight: '600' }}>
        More food type as you can choose
      </Typography>
      <Category />
    </Stack>
  );
};

export default Home;
