import { useContext } from 'react';
import InputTextMessage from '../../component/inputTextMessage';
import { Box, Link, Grid, useMediaQuery, Button, Container, Typography } from '@mui/material';
import { loginContext } from '../../app-context/LoginContext';
import { useHistory } from 'react-router-dom';
import React from 'react';
import axios from 'axios';

const apiUrl = process.env.REACT_APP_LOGIN

const Login = () => {
  const { setLogin } = useContext(loginContext);
  const history = useHistory();
  const isMobile = useMediaQuery("(max-width:900px)"); // Media query for mobile devices

  //tes
  // State to store form values
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  // State to handle form validation
  const [emailError, setEmailError] = React.useState(false);
  const [passwordError, setPasswordError] = React.useState(false);

  const handleLogin = () => {
    console.log("email " + email + " pw " + password)
    // Validate email and password
    if (!email.trim()) {
      setEmailError(true);
    } else {
      setEmailError(false);
    }

    if (!password.trim()) {
      setPasswordError(true);
    } else {
      setPasswordError(false);
    }

    // If both email and password are valid, proceed with login
    if (!email.trim() && !password.trim()) {
      axios.post("http://localhost:58682/api/user/login", {
        email: "syahid@gmail.com",
        Password: "1234"
      }).then(function (response) {
        if (response.data === "Login Success") {
          setLogin(true);
          history.push('/');
        }
        console.log(response.data)
      }).catch(function (error) {
        console.log(error)
      })
    }
  };

  return (
    <Container maxWidth="sm">
      <Box sx={{
        marginTop: 20,
        display: "flex",
        alignItems: "center",
        flexDirection: "column"
      }}>
        <Grid container justifyContent="center" alignItems="center" sx={{ mb: 2 }}>
          <Grid item xs={12}>
            <Typography sx={{
              fontFamily: 'Montserrat',
              fontSize: isMobile ? '20px' : '24px',
              fontWeight: '500',
              lineHeight: '29.26px',
              letterSpacing: '0em',
              textAlign: 'left',
            }}>
              Welcome Back! Cheff
            </Typography>
            <Typography sx={{
              fontFamily: 'Montserrat',
              fontSize: isMobile ? '14px' : '16px',
              fontWeight: 400,
              lineHeight: '19.5px',
              letterSpacing: '0em',
              textAlign: 'left',
              color: '#4F4F4F',
              mb: 2,
            }}>
              Please login first
            </Typography>
          </Grid>
        </Grid>

        <InputTextMessage
          id="email"
          label="Email"
          variant="outlined"
          fullWidth
          required
          margin="normal"
          autoFocus
          name="email"
          type="text"
          error={emailError}
          helperText={emailError ? 'Email is required' : ''}
          value={email}  // Bind the value to the email state
          onChange={e => setEmail(e.target.value)}  // Bind the onChange event to setEmail
        />
        <InputTextMessage
          id="password"
          label="Password"
          variant="outlined"
          fullWidth
          required
          margin="normal"
          autoFocus
          name="password"
          type="password"
          error={passwordError}
          helperText={passwordError ? 'Password is required' : ''}
          value={password}
          onChange={e => setPassword(e.target.value)} // Bind the onChange event to setPassword
        />

        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography component="p" variant="body2" sx={{
              fontFamily: 'Montserrat',
              fontSize: '16px',
              fontWeight: 400,
              lineHeight: '19.5px',
              letterSpacing: '0em',
              textAlign: 'left',
              height: '16px',
              width: '230px',
            }}>
              Forget Password?
              <Link href="/resetpass" color="primary" style={{ marginLeft: '2px' }}>
                Click here
              </Link>
            </Typography>
          </Grid>
        </Grid>
        <Button
          type="submit"
          variant="contained"
          size="medium"
          sx={{
            mt: 3, mb: 1, alignSelf: 'flex-end',
            backgroundColor: '#FABC1D',
            color: 'black',
            textTransform: 'none',
            borderRadius: '8px',
            width: isMobile ? "100%" : "140px", // Make the button full width on mobile
            height: '38px',
            padding: isMobile ? '12px' : '10px', // Increase padding on mobile
            display: isMobile ? 'flex' : 'inline-flex', // Change display to flex on mobile
            justifyContent: 'center', // Center the text on mobile
            gap: '10px'
          }}
          onClick={handleLogin}
        >
          Login
        </Button>
        <Grid container justifyContent="center" alignItems="center" sx={{ mt: 7, mb: 1, alignSelf: 'flex-end' }}>
          <Grid item>
            <Typography sx={{
              fontFamily: 'Montserrat',
              fontSize: '16px',
              fontWeight: 400,
              lineHeight: '19px',
              letterSpacing: '0em',
              textAlign: 'center',
              height: '20px',
              width: '230px',
              display: 'inline',
            }}>
              Don't have an account?
              <Link href="/register" color="#2F80ED" style={{ fontFamily: 'Poppins' }}>
                Sign Up Here
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
}

export default Login;
