import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import '../src/App.css';
import Home from './page/home';
import Menu from './page/menu';
import Detail from './page/detail';
import Invoice from './page/invoice';
import DetailInvoice from './page/detailInvoice';
import Login from './page/login';
import Register from './page/register';
import ResetPass from './page/resetPass';
import CreatePass from './page/createPass';
import { loginContext } from './app-context/LoginContext';
import Navbar from './component/navbar'
import Footer from './component/footer'
import EmailConfirm from './page/emailConfirm';
import SuccessPurcase from './page/successPurchase';
import MyClass from './page/myClass';
import Checkout from './page/checkout';


function App() {
	const [login, setLogin] = useState(false)

	useEffect(() => {
		console.log(login)
	})

	return (
		<loginContext.Provider value={{ login, setLogin }}>
			<Router>
				<Navbar />
				<Switch>
					<Route exact path="/">
						<Home />
					</Route>
					<Route exact path="/menu">
						<Menu />
					</Route>
					<Route exact path="/menu/:category_name" component={Menu}/>
					<Route exact path="/detail">
						<Detail />
					</Route>
					<Route path="/detail/:id" component={Detail} />
					<Route exact path="/invoice">
						<Invoice />
					</Route>
					<Route exact path="/detailinvoice">
						<DetailInvoice />
					</Route>
					<Route exact path="/detailinvoice">
						<DetailInvoice />
					</Route>
					<Route exact path="/login">
						<Login />
					</Route>
					<Route exact path="/register">
						<Register />
					</Route>
					<Route exact path="/resetpass">
						<ResetPass />
					</Route>
					<Route exact path="/createpass">
						<CreatePass />
					</Route>
					<Route exact path="/emailconfirm">
						<EmailConfirm />
					</Route>
					<Route exact path="/successpurchase">
						<SuccessPurcase />
					</Route>
					<Route exact path="/myclass">
						<MyClass />
					</Route>
					<Route exact path="/checkout">
						<Checkout/>
					</Route>
				</Switch>
				<Footer />
			</Router>			
		</loginContext.Provider>
	);
}

export default App;


