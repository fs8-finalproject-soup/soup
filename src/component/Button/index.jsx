import React from 'react'
import { Button } from '@mui/material/Button';

const Buttons = (props) => {
  return (
   <Button
   type={props.type}
   variant={props.variant}
   fullWidth={props.fullWidth}
   sx={{mb:3}}
   >
    {props.label}
   </Button>
  );
}

export default Buttons