import React from 'react';
import { Dialog, DialogActions, Button, Typography, Box } from '@mui/material';
import Bni from "../../assets/Bni.png"
import Bca from "../../assets/Bca.png"
import Mandiri from "../../assets/Mandiri.png"
import Gopay from "../../assets/Gopay.png"
import Ovo from "../../assets/Ovo.png"
import Dana from "../../assets/Dana.png"
import { useHistory } from 'react-router-dom';

const paymentMethods = [
  { logo: Gopay, name: 'Gopay' },
  { logo: Ovo, name: 'Ovo' },
  { logo: Dana, name: 'Dana' },
  { logo: Mandiri, name: 'Mandiri' },
  { logo: Bca, name: 'Bca' },
  { logo: Bni, name: 'Bni' },
  // Add more payment methods here if needed
];

const styles = {
  paymentMethodBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: "16px",
    cursor: "pointer",
  },
  paymentMethodLogo: {
    width: "40px",
    height: "40px",
    marginLeft : "10px",
    marginRight :"20px"
  },
  paymentMethodName: {
    fontFamily: "Poppins",
    fontSize: "18px",
    fontWeight: 500,
    
  },
};

const PaymentModal = ({ openDialog, handleCloseDialog }) => {

  const history = useHistory();

    const handlePaymentButtonClick = () => {
      history.push("/successpurchase");
    };

    return (
        <Dialog open={openDialog} onClose={handleCloseDialog} >
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    margin: "0 auto",
                    width: "374px",
                    padding: "20px",
                    backgroundColor: "#fff",
                    borderRadius: "10px",
                    textAlign: "left", // Align all content to the left
                }}
            >
                <Typography
                    sx={{
                        textAlign: "center",
                        fontFamily: "Poppins",
                        fontSize: "20px",
                        fontWeight: 500,
                        marginBottom: "24px",
                        marginLeft: 0, // Reset marginLeft to 0
                    }}
                >
                    Select Payment Method
                </Typography>

                {/* Render payment methods dynamically */}
                {paymentMethods.map((method) => (
                  <Box
                    key={method.name}
                    sx={styles.paymentMethodBox}
                  >
                    <img
                      src={method.logo}
                      alt={`${method.name} Logo`}
                      style={styles.paymentMethodLogo}
                    />
                    <Typography
                      style={styles.paymentMethodName}
                    >
                      {method.name}
                    </Typography>
                  </Box>
                ))}

                {/* Add payment form or payment details here */}
                {/* You can implement the payment form or integrate a payment gateway */}

                <DialogActions>
                    <Button onClick={handleCloseDialog} color="primary">
                        Close
                    </Button>
                    <Button
                        variant="contained"
                        sx={{
                            backgroundColor: "#FABC1D",
                            fontFamily: "Montserrat",
                            minWidth: "130px",
                        }}
                        onClick={handlePaymentButtonClick}
                    >
                        Pay Now
                    </Button>
                </DialogActions>
            </Box>
        </Dialog>
    );
};

export default PaymentModal;
