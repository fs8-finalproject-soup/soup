import React from 'react'
import imgBanner from '../../assets/Banner2.svg'
import { Box, Stack, Typography } from '@mui/material'

const Banner2 = () => {
    return (
        <Stack sx={{ my: '30px' }}>
            <article
                className='article'
                style={{ backgroundImage: `url(${imgBanner})` }}>
                <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                    <Typography sx={{
                        fontSize: '40px',
                        fontWeight: '600',
                        color: 'white'
                    }}>
                        Gets your best benefit
                    </Typography>
                    <Typography sx={{
                        fontSize: '16px',
                        fontWeight: '500',
                        color: 'white',
                        textAlign: 'justify'
                    }}>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam.
                    </Typography>
                </Box>
            </article>
        </Stack>
    )
}

export default Banner2
