import React from 'react';
import imgBanner from '../../assets/Banner.svg';
import { Box, Stack, Typography } from '@mui/material';

const Banner1 = () => {
  return (
    <Stack>
      <article
        className='article'
        style={{
          background: `url(${imgBanner})`,
          backgroundSize: 'cover', // Menyesuaikan gambar untuk mencakup seluruh lebar elemen
          backgroundPosition: 'center', // Menengahkan gambar secara horizontal
          height: '300px', // Mengatur tinggi elemen
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mx: '50px'
        }}
      >
        <Box>
          <Typography
            sx={{
              fontSize: '32px',
              fontWeight: '600',
              color: 'white',
              textAlign: 'center'
            }}
          >
            Be the next great chef
          </Typography>
          <Typography
            sx={{
              fontSize: '24px',
              fontWeight: '400',
              color: 'white',
              textAlign: 'center'
            }}
          >
            We are able to awaken your cooking skills to become a classy chef and ready to dive into the professional world
          </Typography>
        </Box>
      </article>
    </Stack>
  );
};

export default Banner1;
