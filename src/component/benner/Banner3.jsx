import React from 'react'
import imgBanner from '../../assets/Banner3.png'

const Banner3 = () => {
    return (
        <img src={imgBanner} alt='soup' style={{ width: '100%' }}/>
    )
}

export default Banner3
