import React from 'react'
import { Box, Typography } from '@mui/material'

const bxStyle = {
  width: '324px',
  height: '207px',
  border: 1,
  borderRadius: '20px',
  borderColor: '#BDBDBD',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  paddingX: '16px',
  paddingY: '16px',
  marginX: '15px'
}

const texth1 = {
  fontSize: '48px',
  fontWeight: '600',
  color: '#FABC1D',
  textAlign: 'center'
}

const texth2 = {
  fontSize: '16px',
  fontWeight: '500',
  color: '#5B4947',
  textAlign: 'center'
}

const Trend = () => {
  return (
      <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-evenly'
      }}>
        <Box sx={bxStyle}>
          <Typography sx={texth1}>
            200+
          </Typography>
          <Typography sx={texth2}>
            Various cuisines available in professional class
          </Typography>
        </Box>
        <Box sx={bxStyle}>
          <Typography sx={texth1}>
            50+
          </Typography>
          <Typography sx={texth2}>
            A chef who is reliable and has his own characteristics in cooking
          </Typography>
        </Box>
        <Box sx={bxStyle}>
          <Typography sx={texth1}>
            30+
          </Typography>
          <Typography sx={texth2}>
            Cooperate with trusted and upscale restaurants
          </Typography>
        </Box>
      </Box>
  )
}

export default Trend