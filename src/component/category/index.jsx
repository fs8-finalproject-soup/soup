import { Box, Card, CardActionArea, CardContent, CardMedia, Grid, Typography, useMediaQuery } from '@mui/material';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';

const Category = () => {
  const isMobile = useMediaQuery('(max-width: 600px)'); // Gunakan lebar layar 600px sebagai breakpoint mobile
  const history = useHistory();
  const [data, setData] = useState([]);
  const maxItemsToShow = 8;


  const handleClick = (event, category_name) => {
    event.stopPropagation();
    history.push(`/menu/${category_name}`);
    
  };

  const apiUrl = process.env.REACT_APP_GET_CATEGORY;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(apiUrl) // Ganti dengan URL dan jalur yang sesuai dengan konfigurasi Mockoon Anda
        const responseData = await response.json();
        setData(responseData);
      } catch (error) {
        console.error("Error fetching data",error);
      }
    };

    fetchData();
  }, [apiUrl]);



  return (
    <Box
      sx={{
        width: '100%',
        height: 'auto',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center', // Menengahkan secara horizontal
        alignItems: 'center', // Menengahkan secara vertikal
        padding: isMobile ? '0 20px' : '0', // Sesuaikan padding untuk tampilan mobile dan dekstop 
        flexWrap: 'wrap',
      }}
    >
      {data.slice(0, maxItemsToShow).map((item, index) => (
          <Grid key={index} item xs={3} md={1} mb={2}>
            <Card sx={{
            maxWidth: isMobile ? '100%' : '350px',
            margin: isMobile ? '10px 0' : '0 15px', // Sesuaikan jarak antara item untuk tampilan mobile dan dekstop
            flex: isMobile ? '0 0 100%' : '0 0 auto', // Lebar item 100% untuk tampilan mobile, lebar otomatis untuk dekstop
          }}>
              <CardActionArea onClick={handleClick}>
                <CardMedia
                  component='img'
                  image={'/' + item.picture}
                  alt='soup'
                  sx={{
                    height: isMobile ? '150px' : '200px', // Sesuaikan tinggi gambar untuk tampilan mobile dan dekstop
                    objectFit: 'cover',
                  }}
                />
                <CardContent>
                  <Typography sx={{
                    fontFamily: 'Montserrat',
                    color: '#5B4947',
                    fontSize: isMobile ? '16px' : '20px',
                    fontWeight: '600',
                    height: '29px', // Sesuaikan tinggi teks untuk tampilan mobile dan dekstop
                    marginBottom: '25px'

                  }}>
                    {item.category_name}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
    </Box>
  );
};

export default Category;
