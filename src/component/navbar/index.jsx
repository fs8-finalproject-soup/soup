import Logo from "../../assets/Logo.svg";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';
import { AppBar, Box, Button, IconButton, Link, Toolbar, Typography } from '@mui/material';
import { loginContext } from '../../app-context/LoginContext';
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';

const iconMargin = {
    flexGrow: 1,
    mx: 2,
    fontSize: '16px',
    fontWeight: '500',
    textTransform: 'none'
}

const Navbar = () => {
    const { login, setLogin } = useContext(loginContext)
    const history = useHistory();

    const handleLogout = () => setLogin(false)

    const handleHome = () => {
        history.push('/')
    }

    const handleLogin = () => {
        history.push('/login');
    };

    const handleRegister = () => {
        history.push('/register');
    };

    const handleInvoice = () => {
        history.push('/invoice')
    }

    const handleMyClass = ()=>{
        history.push('/myclass')
    }

    const handleCheckout = ()=>{
        history.push('/checkout')
    }

    return (
        <AppBar position='static'>
            <Toolbar sx={{
                backgroundColor: 'white',
                color: 'black',
                display: 'flex',
                justifyContent: 'space-between'
            }}>
                <Box sx={{ ml: 5 }}>
                    <Link onClick={handleHome}>
                        <img src={Logo} alt="" />
                    </Link>
                </Box>

                {login ?
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyItems: 'flex-end'
                    }}>
                        <IconButton
                            size="large"
                            edge="start"
                            aria-label="menu"
                            onClick={handleCheckout}
                            sx={iconMargin}
                        >
                            <ShoppingCartIcon />
                        </IconButton>
                        <Button variant="h7" component="div" sx={iconMargin} onClick={handleMyClass}>
                            MyClass
                        </Button>
                        <Button variant="h7" component="div" sx={iconMargin} onClick={handleInvoice}>
                            Invoice
                        </Button>
                        <IconButton
                            size="large"
                            edge="start"
                            aria-label="menu"
                            sx={iconMargin}
                            style={{ color: '#FABC1D' }}
                        >
                            <PersonIcon />
                        </IconButton>
                        <Typography variant="h5" component="div" sx={iconMargin}>
                            <hr style={{
                                borderColor: 'black',
                                width: '20px',
                                rotate: '90deg'
                            }} />
                        </Typography>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={iconMargin}
                            onClick={handleLogout}
                        >
                            <LogoutIcon />
                        </IconButton>
                    </Box>
                    :
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyItems: 'flex-end'
                    }}>
                        <Button variant="outlined" component="div" sx={iconMargin}
                            style={{
                                borderRadius: '8px',
                                backgroundColor: '#FFFFFF',
                                borderColor: '#5B4947',
                                color: '#5B4947',
                                width: '175px',
                                height: '40px'
                            }}
                            onClick={handleLogin}>
                            Login
                        </Button>
                        <Button variant="outlined" component="div" sx={iconMargin}
                            style={{
                                borderRadius: '8px',
                                backgroundColor: '#FABC1D',
                                borderColor: '#FABC1D',
                                color: '#5B4947',
                                width: '175px',
                                height: '40px'
                            }}
                            onClick={handleRegister}>
                            Register
                        </Button>

                    </Box>
                }
            </Toolbar>
        </AppBar>
    )
}

export default Navbar
