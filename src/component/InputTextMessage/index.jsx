import React, { useState } from 'react';
import TextField from '@mui/material/TextField';

const InputTextMessage = (props) => {
  const { id, label, variant, fullWidth, required, margin, autoFocus, name, type } = props;
  const [error, setError] = useState(false);
  const [helperText, setHelperText] = useState('');

  // Function to validate the input value
  const validateInput = value => {
    if (required && value.trim() === '') {
      setError(true);
      setHelperText('This field is required');
    } else {
      setError(false);
      setHelperText('');
    }
  };

  const handleChange = event => {
    const inputValue = event.target.value;
    validateInput(inputValue);

    // Additional email validation
    if (name === 'email') {
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (inputValue.trim() !== '' && !emailRegex.test(inputValue)) {
        setError(true);
        setHelperText('Invalid email address');
      }
    }

    // Additional password validation
    if (name === 'password') {
      const correctPassword = 'example'; // Replace with your correct password
      if (inputValue !== correctPassword) {
        setError(true);
        setHelperText('Incorrect password');
        return; // Stop further execution of the function if password is incorrect
      }
    }
  };

  return (
    <TextField
      id={id}
      label={label}
      variant={variant}
      fullWidth={fullWidth}
      required={required}
      error={error}
      helperText={helperText}
      margin={margin}
      autoFocus={autoFocus}
      name={name}
      type={type}
      autoComplete="off" // Disable browser autocomplete for password fields
      onChange={handleChange} // Handle changes and perform validation
    />
  );
};

export default InputTextMessage;
