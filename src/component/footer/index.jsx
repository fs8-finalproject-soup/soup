import { Box, Grid, Link, Typography, useMediaQuery } from '@mui/material';
import React from 'react';
import { LocalPhone, Instagram, YouTube, Telegram, Email } from '@mui/icons-material';

const typoh1 = {
  color: '#FABC1D',
  fontWeight: 500,
  fontSize: '16px',
  marginBottom: 1
};

const typoh2 = {
  color: 'white',
  fontWeight: 400,
  fontSize: '14px',
  textAlign: 'justify',
};

const type = [
  {
    id: 1,
    name: 'Asian'
  },
  {
    id: 2,
    name: 'Dessert'
  },
  {
    id: 3,
    name: 'Cookies'
  },
  {
    id: 4,
    name: 'Cold Drink'
  },
  {
    id: 5,
    name: 'Eastern'
  },
  {
    id: 6,
    name: 'Hot Drink'
  },
  {
    id: 7,
    name: 'Junkfood'
  },
  {
    id: 8,
    name: 'Western'
  }
];

const Footer = () => {
  const iconData = [
    {
      id: 1,
      icon: <LocalPhone />
    },
    {
      id: 2,
      icon: <Instagram />
    },
    {
      id: 3,
      icon: <YouTube />
    },
    {
      id: 4,
      icon: <Telegram />
    },
    {
      id: 5,
      icon: <Email />
    },
  ];

  const isMobile = useMediaQuery("(max-width:900px)"); // Media query for mobile devices

  return (
    <Box sx={{
      backgroundColor: '#5B4947',
      marginTop: '200px',
      paddingX: '70px',
      paddingY: '24px',
      display: 'flex',
      flexDirection: isMobile ? 'column' : 'row', // Mengubah tata letak sesuai dengan perangkat
      justifyContent: 'space-around',
    }}>
      <Grid container columns={{ xs: 3, sm: 8, md: 12 }} sx={{ border: '1px solid black' }} >
        <Grid item xs={12} md={4} sx={{ marginBottom: isMobile ? '20px' : '0' }}>
          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: isMobile ? 'center' : 'flex-start', width: isMobile ? '100%' : '350px' }}>
            <Typography sx={typoh1}>
              About Us
            </Typography>
            <Typography sx={typoh2}>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={12} md={4} sx={{ marginBottom: isMobile ? '20px' : '0' }}>
  <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: isMobile ? 'center' : 'flex-start', width: isMobile ? '100%' : '350px' }}>
    <Typography sx={typoh1}>
      Product
    </Typography>
    <Box>
      <ul>
        <Grid container columnSpacing={10} columns={isMobile ? 1 : { md: 5 }}>
          {type.map((item) => (
            <Grid key={item.id} item md={isMobile ? 12 : 2}>
              <Box>
                <Link href='#' style={{ textDecoration: 'none' }}>
                  <Typography sx={typoh2}>
                    <li>{item.name}</li>
                  </Typography>
                </Link>
              </Box>
            </Grid>
          ))}
        </Grid>
      </ul>
    </Box>
  </Box>
</Grid>
        <Grid item xs={12} md={4}>
          <Box sx={{ width: '350px', marginBottom: '10px' }}>
            <Typography sx={typoh1}>
              Address
            </Typography>
            <Typography sx={typoh2}>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium
            </Typography>
          </Box>
          <Box>
            <Typography sx={typoh1}>
              Contact Us
            </Typography>
            <Box sx={{ display: 'flex', justifyContent: isMobile ? 'center' : 'flex-start' }}>
              {iconData.map((item) => (
                <Link key={item.id} href='#'>
                  <Box sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '48px',
                    height: '48px',
                    color: '#5B4947',
                    backgroundColor: '#FABC1D',
                    borderRadius: '100%',
                    marginRight: isMobile ? '8px' : '16px',
                    marginBottom: isMobile ? '8px' : '0',
                  }}>
                    {item.icon}
                  </Box>
                </Link>
              ))}
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}

export default Footer;
