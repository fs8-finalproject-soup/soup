import { Box, Card, CardActionArea, CardContent, CardMedia, Grid, Typography, useMediaQuery } from '@mui/material';
// import { item1, item2, item3, item4, item5 } from '../../assets';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';



const Content = () => {
  const isMobile = useMediaQuery('(max-width: 600px)'); // Gunakan lebar layar 600px sebagai breakpoint mobile
  const [data, setData] = useState([]);
  const history = useHistory();
  const maxItemsToShow = 6;
 
  
  const handleClick = (event, id) => {
    event.stopPropagation();
    history.push(`/detail/${id}`);
  };

  const apiUrl =process.env.REACT_APP_GET_PRODUCT;
  
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(apiUrl) // Ganti dengan URL dan jalur yang sesuai dengan konfigurasi Mockoon Anda
        const responseData = await response.json();
        setData(responseData);
      } catch (error) {
        console.error("Error fetching data",error);
      }
    };

    fetchData();
  }, [apiUrl]);

 

  return (
    <Box sx={{
      width: '100%',
      height: 'auto',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: isMobile ? 'center' : 'space-between', // Menengahkan secara vertikal saat mobile, dan sejajar ke pinggir saat dekstop
      alignItems: 'center', // Menengahkan secara vertikal
      padding: isMobile ? '0 20px' : '0' // Sesuaikan padding untuk tampilan mobile dan dekstop
      
    }}>
      <Grid container spacing={isMobile ? 1 : 2} justifyContent="center" columns={{ xs: 4, sm: 8, md: 12 }} sx={{ margin: '0 auto', paddingX: '30px'}}>
        {data.slice(0, maxItemsToShow).map((item, index) => (
          <Grid key={index} item xs={3} md={4} sx={{ margin: '0 auto' }}>
            <Card sx={{
              maxWidth: isMobile ? '100%' : '350px',
              padding: isMobile ? '16px' : '0', // Sesuaikan padding untuk tampilan mobile dan dekstop
            }}>
              <CardActionArea onClick={(event) => handleClick(event, item.id)}>
                <CardMedia
                  component='img'
                  height="140"
                  image={'/' + item.picture}
                  alt='soup'
                  sx={{ height: isMobile ? '150px' : '200px' }} // Sesuaikan tinggi gambar untuk tampilan mobile dan dekstop
                />
                <CardContent>
                  <Typography sx={{
                    color: '#828282',
                    fontSize: isMobile ? '12px' : '16px', // Sesuaikan ukuran teks untuk tampilan mobile dan dekstop
                    fontWeight: '400'
                  }}>
                    {item.category}
                  </Typography>
                  <Typography sx={{
                    color: '#5B4947',
                    fontSize: isMobile ? '16px' : '20px',
                    fontWeight: '600',
                    height: isMobile ? 'auto' : '70px', // Sesuaikan tinggi teks untuk tampilan mobile dan dekstop
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: '-webkit-box',
                    '-webkit-line-clamp': isMobile ? 2 : 3, // Batasi jumlah baris teks untuk tampilan mobile dan dekstop
                    '-webkit-box-orient': 'vertical'
                  }}>
                  {item.name}
                  </Typography>
                  <Typography sx={{
                    color: '#FABC1D',
                    fontSize: isMobile ? '16px' : '20px', // Sesuaikan ukuran teks untuk tampilan mobile dan dekstop
                    fontWeight: '600'
                  }}>
                    {`IDR ${item.price}`}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default Content;
